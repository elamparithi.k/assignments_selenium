package AssignmentSelenium;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class AddUser {
	public static void main(String[] args) throws InterruptedException, AWTException {
		WebDriver driver = new ChromeDriver();
		driver.get("https://opensource-demo.orangehrmlive.com");
		driver.manage().window().maximize();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("Admin");
		driver.findElement(By.xpath("//input[@type='password']")).sendKeys("admin123");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//span[text()='Admin']")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//span[text()='User Management ']")).click();
		WebElement user = driver.findElement(By.xpath("//a[text()='Users']"));
		Actions act=new Actions(driver);
		act.moveToElement(user).click().build().perform();
		driver.findElement(By.xpath("//button[text()=' Add ']")).click();
		Thread.sleep(3000);
	 driver.findElement(By.xpath("(//div[@class='oxd-select-text--after'])[1]")).click();
	 Robot r1 = new Robot();
		r1.keyPress(KeyEvent.VK_DOWN);
		r1.keyRelease(KeyEvent.VK_DOWN);
		r1.keyPress(KeyEvent.VK_DOWN);
		r1.keyRelease(KeyEvent.VK_DOWN);

		r1.keyPress(KeyEvent.VK_ENTER);
		r1.keyRelease(KeyEvent.VK_ENTER);

		
    	Thread.sleep(3000);
    	 driver.findElement(By.xpath("(//div[@class='oxd-select-text--after'])[2]")).click();
    	 Robot r2 = new Robot();
 		r2.keyPress(KeyEvent.VK_DOWN);
 		r2.keyRelease(KeyEvent.VK_DOWN);
 		r2.keyPress(KeyEvent.VK_DOWN);
 		r2.keyRelease(KeyEvent.VK_DOWN);

 		r2.keyPress(KeyEvent.VK_ENTER);
 		r2.keyRelease(KeyEvent.VK_ENTER);
 	
    	Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@placeholder='Type for hints...']")).sendKeys("c");
		Robot r4 = new Robot();
		r4.keyPress(KeyEvent.VK_DOWN);
		Thread.sleep(2000);

		r4.keyRelease(KeyEvent.VK_DOWN);
		Thread.sleep(2000);

		r4.keyPress(KeyEvent.VK_ENTER);
		r4.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("(//input[@class='oxd-input oxd-input--active'])[2]")).sendKeys("ElamparithiK");
		driver.findElement(By.xpath("(//input[@type='password'])[1]")).sendKeys("Elam@2211");
		driver.findElement(By.xpath("(//input[@type='password'])[2]")).sendKeys("Elam@2211");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		driver.findElement(By.xpath("//span[@class='oxd-userdropdown-tab']")).click();
		Thread.sleep(5000);
		WebElement ele = driver.findElement(By.xpath("//ul[@class='oxd-dropdown-menu']//a[contains(text(),'Logout')]"));
		act.moveToElement(ele).click().build().perform();
		
   
	     
	//	WebElement element = driver.findElement(By.xpath("(//div[@class='oxd-select-text oxd-select-text--active oxd-select-text- -error'])[1]"));
		//act.moveToElement(element);*/
		
	}

}
