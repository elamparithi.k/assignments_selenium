package AssignmentSelenium;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class Electronics {
	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com");
		driver.manage().window().maximize();
		driver.findElement(By.partialLinkText("Log in")).click();
		driver.findElement(By.id("Email")).sendKeys("elam2211@gmail.com");
	    driver.findElement(By.id("Password")).sendKeys("Elam@2211");
	    driver.findElement(By.xpath("//input[@value='Log in']")).click();
	    WebElement element = driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Electronics')]"));
	    Actions act =new Actions(driver);
	    act.moveToElement(element).build().perform();
	    Thread.sleep(3000);
	    act.moveToElement(driver.findElement(By.partialLinkText("Cell phones"))).click().perform();
	    Thread.sleep(3000);
	    WebElement position = driver.findElement(By.id("products-orderby"));
	    Select sel =new Select(position);
	    sel.selectByIndex(3);
	    driver.findElement(By.xpath("(//input[@value='Add to cart'])[2]")).click();
	    Thread.sleep(3000);
	    driver.findElement(By.partialLinkText("Shopping cart")).click();
	    driver.findElement(By.id("termsofservice")).click();
	    driver.findElement(By.id("checkout")).click();
	    
	    driver.findElement(By.xpath("(//input[@value='Continue'])[1]")).click();
	    Thread.sleep(2000);
	    driver.findElement(By.id("PickUpInStore")).click();
	    Thread.sleep(2000);
	    driver.findElement(By.xpath("(//input[@value='Continue'])[2]")).click();
	    Thread.sleep(2000);
	    driver.findElement(By.xpath("(//input[@value='Continue'])[4]")).click();
	    Thread.sleep(2000);
	    driver.findElement(By.xpath("(//input[@value='Continue'])[5]")).click();
	    Thread.sleep(2000);
	    driver.findElement(By.xpath("//input[@value='Confirm']")).click();
	    Thread.sleep(2000);
	    driver.findElement(By.xpath("//ul[@class='details']//a[contains(text(),'Click here for order details')]")).click();
	    List<WebElement> prnt = driver.findElements(By.xpath("//div[@class='page order-details-page']"));
        for (WebElement ele : prnt) {
        	System.out.println(ele.getText());
			
		}
        driver.findElement(By.partialLinkText("Log out")).click();
	    
		
	}

}
